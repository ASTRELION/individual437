# Project 1

## Installation & Running

### Running via Visual Studio

1. Clone or download the repository to your computer
2. Change to the project directory; `cd individual437/project1/`
3. Open `project1.sln` to open the Visual Studio solution
4. Click the green triangle labeled 'project1' to build and run the game
5. Collect as many asteroids as you can before the sun explodes!

### Running via executable
1. Clone or download the repository to your computer
2. Change to the project directory; `cd individual437/project1/`
3. Open `project1.sln` to open the Visual Studio solution
4. Right-click on the project and select 'Publish...'
5. Complete the wizard to create a local executable program
6. Locate and run the created executable
7. Collect as many asteroids as you can before the sun explodes!

## Requirements

*The following is the list of requirements for this project. Under each is a short description of how my project meets the requirement.*

* [x] 1. The play area is bounded by lines that are at least 3 pixels wide.
    - **4 rectangles 3 pixels wide bound the play area. Total screen size is 1024+6 x 768+6 to account for the bounds.**
* [x] 2. There is a single player-controlled character that is controlled by the keyboard.
    - **The player is represented by a single spaceship with keyboard controls.**
* [x] 3. Movement is controlled by the a, w, d, and s keys (a represents left, w represents up, d represents right, and s represents down). When one of these keys is in the pressed position, a force is applied in that direction.  You may also extend this to use the arrow keys on the keyboard or the dpad on a game controller.
    - **w accelerates the ship, s accelerates the ship in the reverse direction, a rotates the ship counter-clockwise, d rotates the ship clockwise. Controls extend to the arrow keys and a game controller.**
* [x] 4. There is a maximum velocity that a player may achieve beyond which the player cannot move faster.
    - **The spaceship has a maximum move speed of 500 with an acceleration of 10. Acceleration is applied in `update()` and is multiplied by elapsed gameTime.**
* [x] 5. There is a constant force onthe player’s character that always acts in the opposite direction of the character’s movement direction. This, in effect, causes a drag, so that left alone, the player’s character will come to a stop.
    - **When there is no player input, a drag is applied to slow the ship within a window of ~50 movespeed of stopping. This gives the effect of the spaceship floating through space instead of stopping completely. Stopping completely can still be achieved by manually slowing the ship more.**
* [x] 6. The game must play at the same speed independent of CPU or graphics processor speed, within reason.
    - **Acceleration and rotation are multiplied by elapsed gameTime to account for speed of the processor. Similarly, the countdown clock uses gameTime to calculate the time remaining instead of something like DateTime for a more accurate result.**
* [x] 7. The game’s goal is for the character to “pick up” as many targets as possible.  By “pick up” we mean “collide with.”
    - **The player collides with asteroids which are picked up and increment the score by 1. When an asteroid is picked up, it is removed a new one randomly spawns.**
* [x] 8. At any given time, there are 3 targets in the play area.  If a the character picks up the target, then that target is removed from the play area, and a new target created in a random position.  At the beginning of the game, the three targets are placed randomly in the play area.
    - **3 asteroids start in the play area, each being able to spawn a new one once picked up. See 7.**
* [x] 9. You must choose appropriate images for the character and targets.
    - **The spaceship is a 1024x1024 .png image made in [Inkscape](https://inkscape.org/) of a basic spaceship, the asteroids follow similar guidelines. The rectangles for bounds were made with a single white pixel expanded to the space. The font for score and time was found on [dafont.com](https://www.dafont.com) for free [here](https://www.dafont.com/nasalization.font).**
* [x] 10. The game lasts for 60 seconds, and the time remaining is displayed to the user.
    - **A start window of ~5 seconds is given to the player. After 5 seconds, a timer of 60 seconds starts and the player is able to move. When time is up, movement is revoked and the game ends.**
* [x] 11. The game displays the total number of targets captured at the end of the game.
    - **The score can be seen at the end of the game when the timer reaches 0.**
* [x] 12. If the character hits a boundary or wall, then the character may either bounceappropriately or stop all motion.  This is your choice.
    - **If the player collides with a wall, they lose control of their ship for about a second and bounce off spinning.**
* [x] 13. Your source code must be well documented and include a readme file that explains how to build and run your program.  If you have added extensions or other extra features to your game, this is the place to describe them.
    - **You're reading it :)**
