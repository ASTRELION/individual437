﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Project1
{
    public class Asteroid : DrawableGameComponent
    {
        private Game1 game;
        private ContentManager content;
        private Ship ship;

        private readonly int PLAY_WIDTH;
        private readonly int PLAY_HEIGHT;

        private Texture2D texture;
        private Vector2 textureCenter;
        private Rectangle rectangle;
        private Vector2 rectangleCenter;
        private float rotation;
        private float rotationSpeed = 3f;

        public Asteroid(Game1 game) : base(game)
        {
            this.game = game;
            this.content = game.Content;
            this.ship = game.ship;

            this.PLAY_WIDTH = game.PLAY_WIDTH;
            this.PLAY_HEIGHT = game.PLAY_HEIGHT;
        }

        public override void Initialize()
        {
            Random random = new Random();
            this.rectangle = new Rectangle(random.Next(25, PLAY_WIDTH - 25), random.Next(25, PLAY_HEIGHT - 25), 50, 50);
            this.rectangleCenter = Vector2.Divide(this.rectangle.Size.ToVector2(), 2);
            this.rotationSpeed *= (float)(random.NextDouble() - 0.5);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.texture = content.Load<Texture2D>("asteroid");
            this.textureCenter = new Vector2(this.texture.Width / 2, this.texture.Height / 2);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            this.rotation += (float)(this.rotationSpeed * gameTime.ElapsedGameTime.TotalSeconds);
            
            if (this.ship.getRectangle().Intersects(this.rectangle))
            {
                this.game.incrementScore(this);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            // this.game.spriteBatch.Begin();

            this.game.spriteBatch.Draw(this.texture, this.rectangle, null, Color.White, this.rotation, this.textureCenter, SpriteEffects.None, 0f);

            // this.game.spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
