﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project1
{
    public class Util
    {
        public static float degreeToRadian(float angle)
        {
            return ((float)Math.PI / 180f) * angle;
        }
    }
}
