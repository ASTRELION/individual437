﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Project1
{
    public class Ship : DrawableGameComponent
    {
        private Game1 game;

        private ContentManager content;
        private ControlsManager controlsManager;

        private readonly int PLAY_WIDTH;
        private readonly int PLAY_HEIGHT;

        private Texture2D texture;
        private Vector2 textureCenter;
        private Rectangle rectangle;
        private Vector2 rectangleCenter;
        private float rotation;
        private Vector2 direction;
        private Vector2 lastDirection;
        
        private readonly float minMoveSpeed;
        private readonly float maxMoveSpeed;
        private float movespeed;
        private float acceleration;
        private float drag;
        private float rotationSpeed;

        public Ship(Game1 game) : base(game)
        {
            this.game = game;
            this.controlsManager = game.controlsManager;
            this.content = game.Content;

            this.PLAY_WIDTH = game.PLAY_WIDTH;
            this.PLAY_HEIGHT = game.PLAY_HEIGHT;
            
            
            this.rotation = -(float)(Math.PI / 2);
            this.direction = new Vector2(0, -1);
            this.lastDirection = this.direction;

            this.minMoveSpeed = 50f; // drag "dead zone"
            this.maxMoveSpeed = 500f;
            this.movespeed = 0f;
            this.acceleration = 20f;
            this.drag = 5f;
            this.rotationSpeed = 5f;
        }

        public override void Initialize()
        {
            this.rectangle = new Rectangle(this.PLAY_WIDTH / 2, this.PLAY_HEIGHT / 2, 50, 50);
            this.rectangleCenter = Vector2.Divide(this.rectangle.Size.ToVector2(), 2);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.texture = this.content.Load<Texture2D>("ship");
            this.textureCenter = new Vector2(this.texture.Width / 2, this.texture.Height / 2);

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            if (this.game.gameState == Game1.GameState.Playing)
            {
                // rotate ship
                if (controlsManager.moveDirection.X != 0f)
                {
                    this.rotation += (controlsManager.moveDirection.X > 0f ? this.rotationSpeed : -this.rotationSpeed) * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    this.direction.X = (float)Math.Cos(this.rotation);
                    this.direction.Y = (float)Math.Sin(this.rotation);

                    if (this.rotation >= 2f * Math.PI)
                    {
                        this.rotation = (float)(this.rotation % (2f * Math.PI));
                    }
                }
                else if (this.controlsManager.isDisabled())
                {
                    this.rotation += this.rotationSpeed * 2f * (float)gameTime.ElapsedGameTime.TotalSeconds;
                    this.direction.X = (float)Math.Cos(this.rotation);
                    this.direction.Y = (float)Math.Sin(this.rotation);

                    if (this.rotation >= 2f * Math.PI)
                    {
                        this.rotation = (float)(this.rotation % (2f * Math.PI));
                    }
                }

                if (controlsManager.moveDirection.Y != 0f) // accelerate
                {
                    this.lastDirection = this.direction;
                    this.movespeed = Math.Abs(this.movespeed) >= this.maxMoveSpeed ? this.movespeed : this.movespeed + (this.acceleration * -controlsManager.moveDirection.Y);
                }
                else // apply drag
                {
                    if (Math.Abs(this.movespeed) > this.minMoveSpeed)
                    {
                        this.movespeed = this.movespeed > 0f ? this.movespeed - this.drag : this.movespeed + this.drag;
                    }
                }

                // always apply motion (drag needs to be applied every update)
                Vector2 distanceToMove = Vector2.Multiply(Vector2.Multiply(this.lastDirection, movespeed), (float)gameTime.ElapsedGameTime.TotalSeconds);
                distanceToMove.Round();
                Vector2 newLocation = Vector2.Add(this.rectangle.Location.ToVector2(), distanceToMove);

                if (this.game.isInPlayspace(new Rectangle(newLocation.ToPoint(), this.rectangle.Size)))
                {
                    // bounce
                    this.controlsManager.disableForDuration(1000f);
                    this.lastDirection = Vector2.Multiply(this.lastDirection, -1); // reverse direction
                    this.movespeed *= 0.75f; // bounce force

                    distanceToMove = Vector2.Multiply(Vector2.Multiply(this.lastDirection, (float)Math.Ceiling(movespeed)), (float)gameTime.ElapsedGameTime.TotalSeconds);
                    distanceToMove.Round();
                    newLocation = Vector2.Add(this.rectangle.Location.ToVector2(), distanceToMove);

                    this.rectangle.Location = newLocation.ToPoint();
                }

                this.rectangle.Location = newLocation.ToPoint();
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            // this.game.spriteBatch.Begin();

            this.game.spriteBatch.Draw(this.texture, this.rectangle, null, Color.White, this.rotation, this.textureCenter, SpriteEffects.None, 0f);

            // this.game.spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Get the ships positions vector.
        /// </summary>
        /// <returns>The ship's position vector</returns>
        public Vector2 getPosition()
        {
            return this.rectangle.Location.ToVector2();
        }

        /// <summary>
        /// Get the ship's rectangle.
        /// </summary>
        /// <returns>The ship's rectangle</returns>
        public Rectangle getRectangle()
        {
            return this.rectangle;
        }
    }
}
