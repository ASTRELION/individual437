﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Project1
{
    public class Game1 : Game
    {
        public ControlsManager controlsManager { get; set; }

        public Ship ship { get; set; }
        public List<Asteroid> asteroids { get; set; }
        public Score score { get; set; }
        public Time time { get; set; }

        public enum GameState
        {
            Start,
            Playing,
            End
        }

        public GameState gameState;

        public readonly int PLAY_WIDTH = 1024;
        public readonly int PLAY_HEIGHT = 768;

        public readonly int BORDER_WIDTH = 3;

        public readonly int WINDOW_WIDTH;
        public readonly int WINDOW_HEIGHT;

        public SpriteBatch spriteBatch { get; set; }

        private GraphicsDeviceManager _graphics;

        private Texture2D pixelTexture;
        public Rectangle[] walls { get; } = new Rectangle[4];

        private readonly int STAR_COUNT = 50;
        private Texture2D starTexture;
        private Rectangle[] stars;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            controlsManager = new ControlsManager(this);

            this.WINDOW_WIDTH = this.PLAY_WIDTH + (2 * this.BORDER_WIDTH);
            this.WINDOW_HEIGHT = this.PLAY_HEIGHT + (2 * this.BORDER_WIDTH);

            this.asteroids = new List<Asteroid>();
            this.stars = new Rectangle[this.STAR_COUNT];

            this.controlsManager = new ControlsManager(this);
            this.ship = new Ship(this);
            this.score = new Score(this);
            this.time = new Time(this);

            for (int i = 0; i < 3; i++)
            {
                this.spawnAsteroid();
            }

            this.Components.Add(this.ship);
            this.Components.Add(this.controlsManager);
            this.Components.Add(this.score);
            this.Components.Add(this.time);

            this.gameState = GameState.Start;

            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Increments the score, removes the asteroid, and spawns a new one.
        /// </summary>
        /// <param name="component">The Asteroid to remove</param>
        public void incrementScore(Asteroid component)
        {
            this.asteroids.Remove(component);
            this.Components.Remove(component);
            component.Dispose();
            this.score.incrementScore();
            this.spawnAsteroid();
        }

        /// <summary>
        /// Spawns a new Asteroid on the screen.
        /// </summary>
        /// <returns>The Asteroid that was spawned</returns>
        public Asteroid spawnAsteroid()
        {
            Asteroid asteroid = new Asteroid(this);
            this.asteroids.Add(asteroid);
            this.Components.Add(asteroid);
            return asteroid;
        }

        /// <summary>
        /// Change the GameState to Playing.
        /// </summary>
        public void play()
        {
            this.gameState = GameState.Playing;
        }

        /// <summary>
        /// Change the GameState to End.
        /// </summary>
        public void end()
        {
            this.gameState = GameState.End;
        }

        /// <summary>
        /// Checks if the given Rectangle is in the play area.
        /// </summary>
        /// <param name="rect">The Rectangle to check</param>
        /// <returns>True if the Rectangle is in the play area, false otherwise</returns>
        public bool isInPlayspace(Rectangle rect)
        {
            return rect.X - (rect.Width / 2) <= this.BORDER_WIDTH ||
                rect.X + (rect.Width / 2) >= this.PLAY_WIDTH - this.BORDER_WIDTH ||
                rect.Y - (rect.Height / 2) <= this.BORDER_WIDTH ||
                rect.Y + (rect.Height / 2) >= this.PLAY_HEIGHT - this.BORDER_WIDTH;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            _graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            _graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;
            _graphics.ApplyChanges();

            walls[0] = new Rectangle(0, 0, this.BORDER_WIDTH, WINDOW_HEIGHT);
            walls[1] = new Rectangle(WINDOW_WIDTH - this.BORDER_WIDTH, 0, this.BORDER_WIDTH, WINDOW_HEIGHT);
            walls[2] = new Rectangle(0, 0, WINDOW_WIDTH, this.BORDER_WIDTH);
            walls[3] = new Rectangle(0, WINDOW_HEIGHT - this.BORDER_WIDTH, WINDOW_WIDTH, this.BORDER_WIDTH);

            Random random = new Random();
            for (int i = 0; i < this.stars.Length; i++)
            {
                int randWidth, randHeight;
                randWidth = randHeight = random.Next(1, 5);
                int randX = random.Next(randWidth, this.WINDOW_WIDTH - randWidth);
                int randY = random.Next(randHeight, this.WINDOW_HEIGHT - randHeight);
                this.stars[i] = new Rectangle(randX, randY, randWidth, randHeight);
            }

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            pixelTexture = Content.Load<Texture2D>("pixel");
            starTexture = Content.Load<Texture2D>("circle");
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            // draw walls
            foreach (Rectangle r in walls)
            {
                spriteBatch.Draw(this.pixelTexture, r, Color.White);
            }

            // draw stars
            foreach (Rectangle star in this.stars)
            {
                spriteBatch.Draw(this.starTexture, star, Color.White);
            }

            base.Draw(gameTime); // draw all the other game components

            spriteBatch.End();
        }
    }
}
