﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project1
{
    public class Time : DrawableGameComponent
    {
        private Game1 game;
        private ContentManager content;

        private SpriteFont font;

        private readonly float startTime = 5f;
        private readonly float playTime = 60f;
        private float timeRemaining;

        public Time(Game1 game) : base(game)
        {
            this.game = game;
            this.content = game.Content;

            this.timeRemaining = this.startTime;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.font = this.content.Load<SpriteFont>("font");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            switch (this.game.gameState)
            {
                case Game1.GameState.Start:
                    this.timeRemaining = this.startTime - (float)gameTime.TotalGameTime.TotalSeconds;
                    if (timeRemaining <= 0f)
                    {
                        this.timeRemaining = 0f;
                        this.game.play();
                    }
                    break;

                case Game1.GameState.Playing:
                    this.timeRemaining = this.playTime - (float)gameTime.TotalGameTime.TotalSeconds + this.startTime;
                    if (timeRemaining <= 0f)
                    {
                        this.timeRemaining = 0f;
                        this.game.end();
                    }
                    break;

                case Game1.GameState.End:
                    this.timeRemaining = 0f;
                    break;
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            switch (this.game.gameState)
            {
                case Game1.GameState.Start:
                    this.game.spriteBatch.DrawString(this.font, String.Format("{0}", timeRemaining.ToString("00.00")), new Vector2(this.game.WINDOW_WIDTH / 2, 50), Color.Yellow);
                    break;

                case Game1.GameState.Playing:
                    this.game.spriteBatch.DrawString(this.font, String.Format("{0}", timeRemaining.ToString("00.00")), new Vector2(this.game.WINDOW_WIDTH / 2, 50), Color.Red);
                    break;

                case Game1.GameState.End:
                    this.game.spriteBatch.DrawString(this.font, String.Format("{0}", timeRemaining.ToString("00.00")), new Vector2(this.game.WINDOW_WIDTH / 2, 50), Color.Red);
                    break;
            }

            base.Draw(gameTime);
        }
    }
}
