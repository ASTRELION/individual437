﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project1
{
    public class Score : DrawableGameComponent
    {
        private Game1 game;
        private ContentManager content;

        private SpriteFont font;

        private int score;

        public Score(Game1 game) : base(game)
        {
            this.game = game;
            this.content = game.Content;

            this.score = 0;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            this.font = this.content.Load<SpriteFont>("font");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            // this.game.spriteBatch.Begin();

            this.game.spriteBatch.DrawString(this.font, String.Format("Score: {0}", this.score), new Vector2(50, 50), Color.White, 0f, new Vector2(0, 0), 0.5f, SpriteEffects.None, 0f);
            
            // this.game.spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Increments the player's score.
        /// </summary>
        public void incrementScore()
        {
            if (this.game.gameState == Game1.GameState.Playing)
            {
                this.score++;
            }
        }
    }
}
