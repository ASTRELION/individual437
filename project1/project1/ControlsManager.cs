﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Project1
{
    /// <summary>
    /// Manages controls for the game.
    /// </summary>
    public class ControlsManager : GameComponent
    {
        public Vector2 moveDirection { get; set; }

        private Game1 game;

        private float disabledDuration;

        public ControlsManager(Game1 game) : base(game)
        {
            this.game = game;
            this.disabledDuration = 0;
        }

        /// <summary>
        /// Disable movement controls for the given duration in milliseconds.
        /// </summary>
        /// <param name="duration">The duration in milliseconds</param>
        public void disableForDuration(float duration)
        {
            if (!isDisabled()) // prevents stunlock
            {
                this.disabledDuration = duration;
            }
        }

        /// <summary>
        /// Check if movement controls are currently disabled.
        /// </summary>
        /// <returns>True if controls are disabled, false otherwise</returns>
        public bool isDisabled()
        {
            return this.disabledDuration > 0;
        }

        public override void Update(GameTime gameTime)
        {
            disabledDuration -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                this.Game.Exit();
            }
            
            KeyboardState keyState = Keyboard.GetState();
            Vector2 newMoveDirection = new Vector2(0, 0);

            if (!this.isDisabled() && this.game.gameState == Game1.GameState.Playing)
            {
                if (keyState.IsKeyDown(Keys.W) || keyState.IsKeyDown(Keys.Up) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y < 0)
                {
                    newMoveDirection.Y = -1f;
                }

                if (keyState.IsKeyDown(Keys.S) || keyState.IsKeyDown(Keys.Down) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y > 0)
                {
                    newMoveDirection.Y = 1f;
                }

                if (keyState.IsKeyDown(Keys.A) || keyState.IsKeyDown(Keys.Left) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Right.X < 0)
                {
                    newMoveDirection.X = -1f;
                }

                if (keyState.IsKeyDown(Keys.D) || keyState.IsKeyDown(Keys.Right) || GamePad.GetState(PlayerIndex.One).ThumbSticks.Right.X > 0)
                {
                    newMoveDirection.X = 1f;
                }
            }

            this.moveDirection = newMoveDirection;

            base.Update(gameTime);
        }
    }
}
