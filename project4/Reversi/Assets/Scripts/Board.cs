using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public GameObject boardObject;
    public GameObject piecePrefab;

    private GameObject[,] grid;

    private void Awake()
    {
        grid = new GameObject[8, 8];
    }

    // Start is called before the first frame update
    private void Start()
    {
        float z = -0.5f - ((grid.GetLength(0) / 2f) - 1f);
        const float dX = 1f;
        const float dZ = 1f;

        for (int i = 0; i < grid.GetLength(0); i++)
        {
            float x = -0.5f - ((grid.GetLength(1) / 2f) - 1f);
            
            for (int j = 0; j < grid.GetLength(1); j++)
            {
                Vector3 position = this.transform.position + new Vector3(x, 5, z);
                Instantiate(piecePrefab, position, this.transform.rotation, this.transform);
                x += dX;
            }

            z += dZ;
        }
    }

    // Update is called once per frame
    private void Update()
    {
        
    }
}
