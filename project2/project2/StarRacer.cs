﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using BEPUphysics;

namespace project2
{
    public class StarRacer : Game
    {
        public ControlsManager controlsManager { get; set; }
        public ShipCamera shipCamera { get; set; }

        public Ship ship { get; set; }
        public Ring[] rings { get; set; } = new Ring[7];
        public Score score { get; set; }
        public Time time { get; set; }
        public GameState.State currentState { get; set; }

        // HD                                           HD      FHD     QHD     UHD
        public readonly int WINDOW_WIDTH = 1280;    //  1280    1920    2560    3840
        public readonly int WINDOW_HEIGHT = 720;    //  720     1080    1440    2160
        public readonly float ASPECT_RATIO;

        public SpriteBatch _spriteBatch { get; set; }
        private GraphicsDeviceManager _graphics;

        private Model skyboxModel;
        private float rotation = 0f;

        public StarRacer()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;

            ASPECT_RATIO = (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT;
        }

        protected override void Initialize()
        {
            _graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            _graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;
            _graphics.ApplyChanges();

            Space space = new Space();
            Services.AddService(space);

            this.skyboxModel = Content.Load<Model>("Models\\ssphere");

            // Components
            this.controlsManager = new ControlsManager(this);
            this.shipCamera = new ShipCamera(this);

            // Drawables
            this.ship = new Ship(this);
            for (int i = 0; i < rings.Length; i++)
            {
                rings[i] = new Ring(this);
            }
            this.score = new Score(this);
            this.time = new Time(this);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void Update(GameTime gameTime)
        {
            Services.GetService<Space>().Update((float)gameTime.ElapsedGameTime.TotalSeconds);
            rotation += 0.01f * (float)gameTime.ElapsedGameTime.TotalSeconds;
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            //this._spriteBatch.Begin();

            GraphicsDevice.RasterizerState = RasterizerState.CullNone;

            Matrix[] transforms = new Matrix[this.skyboxModel.Bones.Count];
            this.skyboxModel.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in this.skyboxModel.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.World = transforms[mesh.ParentBone.Index] * Matrix.CreateTranslation(Vector3.Zero) * Matrix.CreateScale(100000f) * Matrix.CreateRotationX(rotation);
                    //effect.View = Matrix.CreateLookAt(cameraPosition, this.position, Vector3.Up);
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathF.PI / 3, this.ASPECT_RATIO, 1f, float.MaxValue);
                }

                mesh.Draw();
            }

            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;

            base.Draw(gameTime);

            //this._spriteBatch.End();
        }
    }
}
