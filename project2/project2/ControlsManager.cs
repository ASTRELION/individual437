﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace project2
{
    public class ControlsManager : GameComponent
    {
        /// <summary>Thrusters</summary>
        public float controlThrottle { get; set; } = 0f;
        /// <summary>Standard turning (usually on y axis)</summary>
        public float controlYaw { get; set; } = 0f;
        /// <summary>Flip away/towards the camera</summary>
        public float controlPitch { get; set; } = 0f;
        /// <summary>Rotate around facing direction</summary>
        public float controlRoll { get; set; } = 0f;

        private StarRacer game;

        public ControlsManager(StarRacer game) : base(game)
        {
            this.game = game;
            game.Components.Add(this);
        }

        public override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || 
                Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                this.game.Exit();
            }

            KeyboardState keyState = Keyboard.GetState();
            GamePadState gamePadStateP1 = GamePad.GetState(PlayerIndex.One);
            this.controlThrottle = 0f;
            this.controlYaw = 0f;
            this.controlPitch = 0f;
            this.controlRoll = 0f;

            // THROTTLE
            if (keyState.IsKeyDown(Keys.W) || gamePadStateP1.Triggers.Left > 0) // up
            {
                this.controlThrottle = 1;
            }
            else if (keyState.IsKeyDown(Keys.S) || gamePadStateP1.Triggers.Right > 0) // down
            {
                this.controlThrottle = -1;
            }
            
            // YAW
            if (keyState.IsKeyDown(Keys.A) || gamePadStateP1.ThumbSticks.Left.X < 0) // left
            {
                this.controlYaw = -1;
            }
            else if (keyState.IsKeyDown(Keys.D) || gamePadStateP1.ThumbSticks.Left.X > 0) // right
            {
                this.controlYaw = 1;
            }
            
            // PITCH
            if (keyState.IsKeyDown(Keys.Up) || gamePadStateP1.ThumbSticks.Right.Y > 0) // down
            {
                // inverted
                this.controlPitch = 1;
            }
            else if (keyState.IsKeyDown(Keys.Down) || gamePadStateP1.ThumbSticks.Right.Y < 0) // up
            {
                // inverted
                this.controlPitch = -1;
            }
            
            // ROLL
            if (keyState.IsKeyDown(Keys.Left) || gamePadStateP1.ThumbSticks.Right.X < 0) // left
            {
                this.controlRoll = -1;
            }
            else if (keyState.IsKeyDown(Keys.Right) || gamePadStateP1.ThumbSticks.Right.X > 0) // right
            {
                this.controlRoll = 1;
            }

            base.Update(gameTime);
        }
    }
}
