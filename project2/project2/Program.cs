﻿using System;

namespace project2
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new StarRacer())
                game.Run();
        }
    }
}
