﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace project2
{
    public class ShipCamera : GameComponent
    {
        public Vector3 position { get; set; } = new Vector3(0, 50, 50);
        public Vector3 relativePosition { get; set; } = new Vector3(0f, 0f, 5f);
        protected float lookAhead = 1000f;

        public ShipCamera(StarRacer game) : base(game)
        {

        }

        public void SetInitialPosition(Vector3 shipPosition)
        {
            this.position = Vector3.Add(shipPosition, relativePosition);
        }

        public float GetLookAhead()
        {
            return this.lookAhead;
        }
    }
}
