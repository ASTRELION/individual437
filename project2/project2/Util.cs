﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace project2
{
    public static class Util
    {
        public static BEPUutilities.Vector3 Vector3ToBepu(Vector3 monoVector)
        {
            return new BEPUutilities.Vector3(monoVector.X, monoVector.Y, monoVector.Z);
        }

        public static Vector3 Vector3ToMono(BEPUutilities.Vector3 bepuVector)
        {
            return new Vector3(bepuVector.X, bepuVector.Y, bepuVector.Z);
        }

        public static Matrix MatrixToMono(BEPUutilities.Matrix bepuMatrix)
        {
            Matrix monoMatrix;
            monoMatrix.M11 = bepuMatrix.M11;
            monoMatrix.M12 = bepuMatrix.M12;
            monoMatrix.M13 = bepuMatrix.M13;
            monoMatrix.M14 = bepuMatrix.M14;

            monoMatrix.M21 = bepuMatrix.M21;
            monoMatrix.M22 = bepuMatrix.M22;
            monoMatrix.M23 = bepuMatrix.M23;
            monoMatrix.M24 = bepuMatrix.M24;

            monoMatrix.M31 = bepuMatrix.M31;
            monoMatrix.M32 = bepuMatrix.M32;
            monoMatrix.M33 = bepuMatrix.M33;
            monoMatrix.M34 = bepuMatrix.M34;

            monoMatrix.M41 = bepuMatrix.M41;
            monoMatrix.M42 = bepuMatrix.M42;
            monoMatrix.M43 = bepuMatrix.M43;
            monoMatrix.M44 = bepuMatrix.M44;
            return monoMatrix;
        }

        public static BEPUutilities.Vector3 QuaternionToVector(BEPUutilities.Quaternion q)
        {
            BEPUutilities.Vector3 forward = BEPUutilities.Vector3.Forward;
            BEPUutilities.Vector3 qVector = new BEPUutilities.Vector3(q.X, q.Y, q.Z);
            BEPUutilities.Vector3 newVector = BEPUutilities.Vector3.Zero;
            //BEPUutilities.Vector3.Multiply(ref qVector, q.W / MathF.Sin(q.W / 2f), out newVector);
            
            newVector.Normalize();
            return qVector;
        }

        public static Vector3 QuaternionToVector(Quaternion q)
        {
            return Vector3.Multiply(new Vector3(q.X, q.Y, q.Z), Vector3.Forward);
        }

        public static Vector3 YawPitchToDirection(float yaw, float pitch)
        {
            return new Vector3(
                -(MathF.Sin(yaw) * MathF.Cos(pitch)),
                MathF.Sin(pitch),
                -(MathF.Cos(yaw) * MathF.Cos(pitch))
            );
        }

        /// <summary>
        /// Convert given vector to yaw and pitch values.
        /// </summary>
        /// <param name="vector">The vector to convert</param>
        /// <returns>A Vector2 with X being Yaw and Y being Pitch</returns>
        public static Vector2 DirectionToYawPitch(Vector3 vector)
        {
            return new Vector2(
                MathF.Atan(vector.X / -vector.Y), 
                MathF.Atan(MathF.Sqrt(MathF.Pow(vector.X, 2f) + MathF.Pow(vector.Y, 2f)) / 2f)
            );
        }

        public static Vector3 Vector3Clone(Vector3 vector)
        {
            float x = vector.X;
            float y = vector.Y;
            float z = vector.Z;
            return new Vector3(x, y, z);
        }
    }
}
