﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace project2
{
    public class StarRacerDrawableComponent : DrawableGameComponent
    {
        protected StarRacer game;
        protected ControlsManager controlsManager;
        protected ContentManager content;
        protected ShipCamera camera;

        public StarRacerDrawableComponent(StarRacer game) : base(game)
        {
            this.game = game;
            this.controlsManager = game.controlsManager;
            this.content = game.Content;
            this.camera = game.shipCamera;

            game.Components.Add(this);
        }
    }
}
