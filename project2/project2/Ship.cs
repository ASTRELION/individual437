﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BEPUphysics;

namespace project2
{
    public class Ship : StarRacerDrawableComponent
    {
        private Model model;
        private BEPUphysics.Entities.Prefabs.Box physics;

        private Vector3 position = Vector3.Zero;
        private Vector3 direction = new Vector3(0, 0, 1);
        private float speed = 100f;
        private float yaw = 0f;
        private float yawSpeed = 1f;
        private float pitch = 0f;
        private float pitchSpeed = 0.5f;
        private float roll = 0f;
        private float rollSpeed = 1f;

        public Ship(StarRacer game) : base(game)
        {
            this.physics = new BEPUphysics.Entities.Prefabs.Box(Util.Vector3ToBepu(this.position), 1, 1, 1);
            this.physics.AngularDamping = 0f;
            this.physics.LinearDamping = 0f;
            this.physics.Tag = "ship";
            this.game.Services.GetService<Space>().Add(this.physics);
            //this.camera.SetInitialPosition(this.position);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            model = this.content.Load<Model>("Models\\StarSparrow");
            this.physics.Width = model.Meshes[0].BoundingSphere.Radius;
            this.physics.Height = model.Meshes[0].BoundingSphere.Radius;
            this.physics.Length = model.Meshes[0].BoundingSphere.Radius;

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            this.yaw += this.controlsManager.controlYaw * this.yawSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            this.pitch += this.controlsManager.controlPitch * this.pitchSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            this.roll += this.controlsManager.controlRoll * this.rollSpeed * (float)gameTime.ElapsedGameTime.TotalSeconds;

            //this.direction = Util.YawPitchToDirection(this.yaw, this.pitch);
            //this.camera.position = Vector3.Add(Vector3.Zero, new Vector3(0, 20f, 5f));

            //this.physics.LinearMomentum = Util.Vector3ToBepu(Util.YawPitchToDirection(this.yaw, this.pitch));
            //this.physics.AngularMomentum = Util.Vector3ToBepu(Util.YawPitchToDirection(this.yaw, this.pitch));
            //this.physics.LinearVelocity = Util.Vector3ToBepu(this.direction);
            /*this.physics.AngularVelocity = Util.Vector3ToBepu(Vector3.Multiply(new Vector3(
                this.controlsManager.controlPitch * this.pitchSpeed,
                this.controlsManager.controlRoll * this.rollSpeed,
                this.controlsManager.controlYaw * this.yawSpeed
            ), (float)gameTime.ElapsedGameTime.TotalMilliseconds));*/

            Matrix m = Matrix.CreateFromYawPitchRoll(roll, pitch, yaw);
            BEPUutilities.Matrix bm = ConversionHelper.MathConverter.Convert(m);
            BEPUutilities.Matrix3x3 bm3 = new BEPUutilities.Matrix3x3();
            this.physics.OrientationMatrix = BEPUutilities.Matrix3x3.CreateFromMatrix(bm);

            //this.direction = Util.Vector3ToMono(Util.QuaternionToVector(this.physics.Orientation));
            //this.direction = Util.Vector3ToMono(this.physics.OrientationMatrix.);
            this.direction = new Vector3(
                MathF.Cos(yaw) * MathF.Cos(pitch),
                MathF.Sin(yaw) * MathF.Cos(pitch),
                MathF.Sin(pitch)
            );

            this.physics.LinearVelocity = Util.Vector3ToBepu(Vector3.Multiply(
                Vector3.Multiply(new Vector3(
                    this.controlsManager.controlThrottle,
                    this.controlsManager.controlThrottle,
                    this.controlsManager.controlThrottle
                ), new Vector3(0, 0, 1)),
            (float)gameTime.ElapsedGameTime.TotalSeconds * this.speed));
            
            this.position = Util.Vector3ToMono(this.physics.Position);

            Debug.WriteLine(
                "Ship Position: " + this.position + "\n" +
                "Ship Direction: " + this.direction + "\n" +
                "Camera Position: " + this.camera.position + "\n" +
                "Quaternion: " + this.physics.Orientation
            );

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Matrix[] transforms = new Matrix[this.model.Bones.Count];
            this.model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in this.model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.Alpha = 1f;
                    effect.EnableDefaultLighting();
                    effect.World = Util.MatrixToMono(this.physics.WorldTransform) * Matrix.CreateScale(1f);
                    effect.View = Matrix.CreateLookAt(this.camera.position, this.position, Vector3.Down);
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(
                        MathF.PI / 4f, 
                        this.game.GraphicsDevice.Viewport.AspectRatio, 
                        0.1f, 
                        1000f
                    );
                }

                mesh.Draw();
            }

            base.Draw(gameTime);
        }

        public Vector3 GetDirection()
        {
            return this.direction;
        }
    }
}
