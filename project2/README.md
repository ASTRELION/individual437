# Star Racer - Project 2

## Installation & Running
1. Download or clone the repository
2. Open the Visual Studio solution
3. Click the green run button

### Controls
W/S: thrust  
A/D: yaw  
[arrow_up]/[arrow_down]: pitch  
[arrow_left]/[arrow_right]: roll

## Requirements + Implementations
1. The game must be written in C# usingMonogame, with 3D mechanics.
2. The game must use the Bepu physics engine to move and control all 3D objects in the game.
3. The game is played as first-person, but a behind-the-ship view is also acceptable.
4. The base game requires that the player use a keyboard or gamepad (or both) to control a spacecraft in 3 dimensionsplus the forward thruster. The available control provides input that controls the yaw, roll, pitch, and thrust of the spacecraft. ( https://en.wikipedia.org/wiki/Aircraft_principal_axes) Control of the spacecraft through the input device is via steering behaviors (not kinematic behaviors) in which only forces areapplied.
5. The base game utilizes rings floating in space in which the player must fly through in a specific order. Since multiple rings may be in view at the same time, accents (changed color or bright glow) must be used to indicate the next ring in the sequence.
6. The game must have at least seven rings to form a course.  The last ring may be the first ring in order to allow a race to have multiple laps. (This is not required.)
7. This is a single player game in which the player races against time to complete the course. A running clock must be displayed to the player, and the final time displayed when the player crosses the finish line.
8. The game tracks and displays the number of rings missed.
9. The game computes a final score based on time and rings missed. This score should be balanced. That is, it should not benefit a player to fly directly to the last ring and skip all the other rings.
10. The rings are “infinite mass” and do not move when hit.•You must create and use a skybox to display a background.