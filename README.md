# COM S 437 Individual Projects

*This repository is mirrored to my personal (and public) GitLab [here](https://gitlab.com/ASTRELION/individual437).*

## [Project 1 | 2D Pickup Game](project1/)

A 2D game made with [Monogame](https://www.monogame.net/)

## [Project 2 | Star Racer](project2/)

A 3D ship simulator made with [Monogame](https://www.monogame.net/)

## [Project 3 | Reversi](project3/)

Console-based implementation of [Reversi](https://en.wikipedia.org/wiki/Reversi), made with raw C#

## [Project 4 | Reversi (Unity)](project4/Reversi/)

Unity implementation of [Reversi](https://en.wikipedia.org/wiki/Reversi)

<img src="project4/Reversi/screenshots/screenshot.png" width="50%" />

## Project 5