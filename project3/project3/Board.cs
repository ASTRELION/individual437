﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project3
{
    public class Board
    {
        /// <summary>
        /// 0: player's turn; 1: computer's turn
        /// </summary>
        public enum TURN {
            X,  // white
            O,  // black
            N   // none/tie
        }

        public TURN turn = TURN.X;

        public int width { get; }
        public int height { get; }

        protected char[,] charBoard;

        protected readonly char EMPTY = '-';
        protected readonly char WHITE = 'x';
        protected readonly char BLACK = 'o';

        public Board(TURN turn, int width = 8, int height = 8)
        {
            this.turn = turn;
            this.width = width;
            this.height = height;
            charBoard = new char[height, width];

            for (int i = 0; i < charBoard.GetLength(0); i++)
            {
                for (int j = 0; j < charBoard.GetLength(1); j++)
                {
                    if ((i == (charBoard.GetLength(0) / 2) - 1 && j == (charBoard.GetLength(1) / 2) - 1) || 
                        (i == (charBoard.GetLength(0) / 2) && j == (charBoard.GetLength(1) / 2)))
                    {
                        charBoard[i, j] = BLACK;
                    }
                    else if ((i == (charBoard.GetLength(0) / 2) && j == (charBoard.GetLength(0) / 2) - 1) || 
                        (i == (charBoard.GetLength(0) / 2) - 1 && j == (charBoard.GetLength(0) / 2)))
                    {
                        charBoard[i, j] = WHITE;
                    }
                    else
                    {
                        charBoard[i, j] = EMPTY;
                    }
                }
            }
        }

        /// <summary>
        /// Get the current turn's symbol
        /// </summary>
        /// <param name="turn">Optional turn to get symbol for</param>
        /// <returns>The turn's symbol</returns>
        public char GetTurnSymbol(TURN? turn = null)
        {
            if (turn == null) turn = this.turn;

            if (turn == TURN.X) return WHITE;
            else return BLACK;
        }

        /// <summary>
        /// Get the opponent's piece symbol
        /// </summary>
        /// <param name="turn">Optional current turn</param>
        /// <returns>The opponent's symbol</returns>
        public char GetOpponentSymbol(TURN? turn = null)
        {
            if (turn == null) turn = this.turn;

            if (turn == TURN.X) return BLACK;
            else return WHITE;
        }

        public TURN GetOpponent(TURN? turn = null)
        {
            if (turn == null) turn = this.turn;

            if (turn == TURN.X) return TURN.O;
            else return TURN.X;
        }

        /// <summary>
        /// Advance the turn state
        /// </summary>
        /// <returns>The new turn</returns>
        public TURN Advance()
        {
            turn = turn == TURN.X ? TURN.O : TURN.X;
            return turn;
        }

        /// <summary>
        /// Get the number of white (x) pieces on the board
        /// </summary>
        /// <returns>The number of white (x) pieces</returns>
        public int GetNumWhite()
        {
            int count = 0;

            foreach (char c in charBoard)
            {
                if (c == this.WHITE) count++;
            }

            return count;
        }

        /// <summary>
        /// Get the number of black (o) pieces on the board
        /// </summary>
        /// <returns>The number of black (o) pieces</returns>
        public int GetNumBlack()
        {
            int count = 0;

            foreach (char c in charBoard)
            {
                if (c == this.BLACK) count++;
            }

            return count;
        }

        /// <summary>
        /// Smartly flip a given board coordinate. Blanks will be flipped to the current turn's piece,
        /// white pieces will be flipped to black pieces, black pieces will be flipped to white pieces
        /// </summary>
        /// <param name="x">The x coordinate to flip</param>
        /// <param name="y">The y coordinate to flip</param>
        public void Flip(int x, int y)
        {
            if (charBoard[y, x] == WHITE) 
                charBoard[y, x] = BLACK;
            else if (charBoard[y, x] == BLACK) 
                charBoard[y, x] = WHITE;
            else 
                charBoard[y, x] = GetTurnSymbol(null);
        }

        /// <summary>
        /// Any piece that isn't the given turn's piece is turned into turn's piece
        /// </summary>
        /// <param name="x">The x coordinate to flip</param>
        /// <param name="y">The y coordinate to flip</param>
        /// <param name="turn">The player turn</param>
        public void Flip(int x, int y, TURN turn)
        {
            if (charBoard[y, x] != GetTurnSymbol(turn))
            {
                charBoard[y, x] = GetTurnSymbol(turn);
            }
        }

        /// <summary>
        /// Check whether the board's current state is terminal (i.e. there are no possible moves left)
        /// </summary>
        /// <returns>true if the board is in a terminal state, false otherwise</returns>
        public bool IsTerminal(TURN? turn = null)
        {
            return GetMoves(turn).Count == 0;
        }

        /// <summary>
        /// Get the winner of the current board (doesn't have to be in terminal state)
        /// </summary>
        /// <returns>The winner of the board</returns>
        public TURN GetWinner()
        {
            int numW = GetNumWhite();
            int numB = GetNumBlack();

            if (numW == numB)
            {
                return TURN.N;
            }
            else if (numW > numB)
            {
                return TURN.X;
            }
            else
            {
                return TURN.O;
            }
        }

        /// <summary>
        /// Get the score for the given player turn
        /// </summary>
        /// <param name="turn">The player to get score for</param>
        /// <returns>The score of the player</returns>
        public int Evaluate(TURN turn)
        {
            if (turn == TURN.X) 
                return GetNumWhite();
            else 
                return GetNumBlack();
        }

        /// <summary>
        /// Gets a list of all possible moves from the current board's state
        /// </summary>
        /// <returns>A List of all possible BoardMoves</returns>
        public HashSet<BoardMove> GetMoves(TURN? turn = null)
        {
            if (turn == null) turn = this.turn;
            HashSet<BoardMove> moves = new HashSet<BoardMove>();

            for (int i = 0; i < charBoard.GetLength(0); i++)
            {
                for (int j = 0; j < charBoard.GetLength(1); j++)
                {
                    if (charBoard[i, j] == EMPTY)
                    {
                        BoardMove move;
                        if (FlipPieces(move = new BoardMove(j, i, (TURN)turn), false))
                        {
                            moves.Add(move);
                        }
                    }
                }
            }

            return moves;
        }

        /// <summary>
        /// Flip pieces in accordance to the given move. There is probably a much more elegant solution than this but brute force is just so ez
        /// </summary>
        /// <param name="move">The move to make</param>
        /// <param name="commit">Whether to commit the changes to the board or not</param>
        /// <returns>true if the move was successful, false otherwise</returns>
        protected bool FlipPieces(BoardMove move, bool commit = true)
        {
            HashSet<(int, int)> piecesToFlip = new HashSet<(int, int)>();
            bool foundOpponentPiece;
            bool validMoveN, validMoveS, validMoveE, validMoveW, validMoveNE, validMoveNW, validMoveSE, validMoveSW;
            validMoveN = validMoveS = validMoveE = validMoveW = validMoveNE = validMoveNW = validMoveSE = validMoveSW = false;

            // HORIZONTAL - //

            foundOpponentPiece = false;
            for (int j = move.x + 1; j < charBoard.GetLength(1); j++) // right
            {
                if (charBoard[move.y, j] == GetOpponentSymbol(move.turn))
                {
                    piecesToFlip.Add((j, move.y));
                    foundOpponentPiece = true;
                }
                else if (charBoard[move.y, j] == GetTurnSymbol(move.turn))
                {
                    if (foundOpponentPiece)
                    {
                        validMoveE = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (charBoard[move.y, j] == EMPTY)
                {
                    break;
                }
            }

            if (validMoveE && commit)
            {
                foreach ((int, int) t in piecesToFlip)
                {
                    Flip(t.Item1, t.Item2, move.turn);
                }
            }

            piecesToFlip.Clear();

            foundOpponentPiece = false;
            for (int j = move.x - 1; j >= 0; j--) // left
            {
                if (charBoard[move.y, j] == GetOpponentSymbol(move.turn))
                {
                    piecesToFlip.Add((j, move.y));
                    foundOpponentPiece = true;
                }
                else if (charBoard[move.y, j] == GetTurnSymbol(move.turn))
                {
                    if (foundOpponentPiece)
                    {
                        validMoveW = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (charBoard[move.y, j] == EMPTY)
                {
                    break;
                }
            }

            if (validMoveW && commit)
            {
                foreach ((int, int) t in piecesToFlip)
                {
                    Flip(t.Item1, t.Item2, move.turn);
                }
            }

            piecesToFlip.Clear();

            // VERTICAL | //
            foundOpponentPiece = false;
            for (int i = move.y + 1; i < charBoard.GetLength(0); i++) // down
            {
                if (charBoard[i, move.x] == GetOpponentSymbol(move.turn))
                {
                    piecesToFlip.Add((move.x, i));
                    foundOpponentPiece = true;
                }
                else if (charBoard[i, move.x] == GetTurnSymbol(move.turn))
                {
                    if (foundOpponentPiece)
                    {
                        validMoveS = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (charBoard[i, move.x] == EMPTY)
                {
                    break;
                }
            }

            if (validMoveS && commit)
            {
                foreach ((int, int) t in piecesToFlip)
                {
                    Flip(t.Item1, t.Item2, move.turn);
                }
            }

            piecesToFlip.Clear();

            foundOpponentPiece = false;
            for (int i = move.y - 1; i >= 0; i--) // up
            {
                if (charBoard[i, move.x] == GetOpponentSymbol(move.turn))
                {
                    piecesToFlip.Add((move.x, i));
                    foundOpponentPiece = true;
                }
                else if (charBoard[i, move.x] == GetTurnSymbol(move.turn))
                {
                    if (foundOpponentPiece)
                    {
                        validMoveN = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (charBoard[i, move.x] == EMPTY)
                {
                    break;
                }
            }

            if (validMoveN && commit)
            {
                foreach ((int, int) t in piecesToFlip)
                {
                    Flip(t.Item1, t.Item2, move.turn);
                }
            }

            piecesToFlip.Clear();

            // DIAGONAL ASCENDING / //

            foundOpponentPiece = false;
            int dj = move.x;
            for (int i = move.y - 1; i >= 0; i--) // right /
            {
                if (++dj >= charBoard.GetLength(1)) break;

                if (charBoard[i, dj] == GetOpponentSymbol(move.turn))
                {
                    piecesToFlip.Add((dj, i));
                    foundOpponentPiece = true;
                }
                else if (charBoard[i, dj] == GetTurnSymbol(move.turn))
                {
                    if (foundOpponentPiece)
                    {
                        validMoveNE = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (charBoard[i, dj] == EMPTY)
                {
                    break;
                }
            }

            if (validMoveNE && commit)
            {
                foreach ((int, int) t in piecesToFlip)
                {
                    Flip(t.Item1, t.Item2, move.turn);
                }
            }

            piecesToFlip.Clear();

            foundOpponentPiece = false;
            dj = move.x;
            for (int i = move.y + 1; i < charBoard.GetLength(0); i++) // left /
            {
                if (--dj < 0) break;

                if (charBoard[i, dj] == GetOpponentSymbol(move.turn))
                {
                    piecesToFlip.Add((dj, i));
                    foundOpponentPiece = true;
                }
                else if (charBoard[i, dj] == GetTurnSymbol(move.turn))
                {
                    if (foundOpponentPiece)
                    {
                        validMoveSW = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (charBoard[i, dj] == EMPTY)
                {
                    break;
                }
            }

            if (validMoveSW && commit)
            {
                foreach ((int, int) t in piecesToFlip)
                {
                    Flip(t.Item1, t.Item2, move.turn);
                }
            }

            piecesToFlip.Clear();

            // DIAGONAL DESCENDING \ //

            foundOpponentPiece = false;
            dj = move.x;
            for (int i = move.y + 1; i < charBoard.GetLength(0); i++) // right \
            {
                if (++dj >= charBoard.GetLength(1)) break;

                if (charBoard[i, dj] == GetOpponentSymbol(move.turn))
                {
                    piecesToFlip.Add((dj, i));
                    foundOpponentPiece = true;
                }
                else if (charBoard[i, dj] == GetTurnSymbol(move.turn))
                {
                    if (foundOpponentPiece)
                    {
                        validMoveSE = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (charBoard[i, dj] == EMPTY)
                {
                    break;
                }
            }

            if (validMoveSE && commit)
            {
                foreach ((int, int) t in piecesToFlip)
                {
                    Flip(t.Item1, t.Item2, move.turn);
                }
            }

            piecesToFlip.Clear();

            foundOpponentPiece = false;
            dj = move.x;
            for (int i = move.y - 1; i >= 0; i--) // left \
            {
                if (--dj < 0) break;

                if (charBoard[i, dj] == GetOpponentSymbol(move.turn))
                {
                    piecesToFlip.Add((dj, i));
                    foundOpponentPiece = true;
                }
                else if (charBoard[i, dj] == GetTurnSymbol(move.turn))
                {
                    if (foundOpponentPiece)
                    {
                        validMoveNW = true;
                        break;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (charBoard[i, dj] == EMPTY)
                {
                    break;
                }
            }

            if (validMoveNW && commit)
            {
                foreach ((int, int) t in piecesToFlip)
                {
                    Flip(t.Item1, t.Item2, move.turn);
                }
            }

            piecesToFlip.Clear();

            return validMoveN || validMoveS || validMoveE || validMoveW ||
                validMoveNE || validMoveNW || validMoveSE || validMoveSW;
        }

        /// <summary>
        /// Check if a move is valid
        /// </summary>
        /// <param name="move">The move to check</param>
        /// <returns>true if the move is valid, false otherwise</returns>
        public bool CheckMove(BoardMove move)
        {
            return FlipPieces(move, false);
        }

        /// <summary>
        /// Place the piece and perform flipping
        /// </summary>
        /// <param name="move">The move to perform</param>
        public void MakeMove(BoardMove move, bool debug = false)
        {
            // flip between all pieces
            FlipPieces(move);
            Flip(move.x, move.y, move.turn); // flip where the move was made

            if (debug) Debug.WriteLine(move);
        }

        public (int, BoardMove) MiniMax(Board board, TURN turn, int maxDepth, int currentDepth = 0)
        {
            int bestScore = 0;
            BoardMove bestMove = null;

            if (board.IsTerminal() || currentDepth >= maxDepth)
            {
                return (board.Evaluate(turn), null);
            }

            if (board.turn == turn)
            {
                bestScore = int.MinValue;
            }
            else
            {
                bestScore = int.MaxValue;
            }

            foreach (BoardMove move in board.GetMoves())
            {
                Board newBoard = board.Clone();
                newBoard.MakeMove(move);
                newBoard.Advance();

                int alpha = board.Evaluate(turn);
                int beta = newBoard.Evaluate(turn);
                
                if (beta > alpha)
                {
                    (int, BoardMove) t = MiniMax(newBoard, turn, maxDepth, currentDepth++);

                    if (board.turn == turn)
                    {
                        if (t.Item1 > bestScore)
                        {
                            bestScore = t.Item1;
                            bestMove = move;
                        }
                    }
                    else
                    {
                        if (t.Item1 < bestScore)
                        {
                            bestScore = t.Item1;
                            bestMove = move;
                        }
                    }
                }
            }

            return (bestScore, bestMove);
        }

        /// <summary>
        /// Create a new copy of this Board
        /// </summary>
        /// <returns>A copy of this board</returns>
        public Board Clone()
        {
            Board newBoard = new Board(this.turn, this.width, this.height);
            
            for (int i = 0; i < newBoard.charBoard.GetLength(0); i++)
            {
                for (int j = 0; j < newBoard.charBoard.GetLength(1); j++)
                {
                    newBoard.charBoard[i, j] = this.charBoard[i, j];
                }
            }

            return newBoard;
        }

        public override string ToString()
        {
            StringBuilder boardString = new StringBuilder(" ");

            for (int w = 0; w < MathF.Floor(MathF.Log10(this.width) + 1); w++)
            {
                boardString.Append(' ');
            }

            for (int j = 0; j < charBoard.GetLength(1); j++)
            {
                boardString.Append($"{j + 1} ");

                for (int w = 0; w < MathF.Floor(MathF.Log10(this.width) + 1) - MathF.Floor(MathF.Log10(j + 1) + 1); w++)
                {
                    boardString.Append(' ');
                }
            }

            boardString.AppendLine();

            for (int i = 0; i < charBoard.GetLength(0); i++)
            {
                boardString.Append($"{i + 1} ");

                for (int h = 0; h < MathF.Floor(MathF.Log10(this.height) + 1) - MathF.Floor(MathF.Log10(i + 1) + 1); h++)
                {
                    boardString.Append(' ');
                }

                for (int j = 0; j < charBoard.GetLength(1); j++)
                {
                    boardString.Append(charBoard[i, j]);

                    for (int w = 0; w < MathF.Floor(MathF.Log10(this.width) + 1); w++)
                    {
                        boardString.Append(' ');
                    }
                }

                if (i == (this.height / 2) - 1)
                {
                    boardString.Append($"\t# x's: {GetNumWhite()}\t(you)");
                }
                else if (i == this.height / 2)
                {
                    boardString.Append($"\t# o's: {GetNumBlack()}");
                }

                boardString.AppendLine();
            }

            return boardString.ToString();
        }
    }
}
