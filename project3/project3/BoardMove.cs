﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project3
{
    public class BoardMove
    {
        public int x { get; set; }
        public int y { get; set; }
        public Board.TURN turn { get; set; }

        public BoardMove(int x, int y, Board.TURN turn)
        {
            this.x = x;
            this.y = y;
            this.turn = turn;
        }

        public override string ToString()
        {
            return $"{turn}: {x},{y}";
        }
    }
}
