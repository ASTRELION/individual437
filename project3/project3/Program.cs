﻿using System;
using System.IO;

namespace project3
{
    public class Program
    {
        public static void Main()
        {
            int difficulty = 10;
            (int, int) dimensions = (8, 8); // width, height
            Board.TURN startTurn = Board.TURN.X;
            bool isAIOnly = false;
            String input;

            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Reversi");
                Console.ResetColor();
                Console.WriteLine();

                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine("Enter the following game options, or press ENTER to accept the default shown in ()\n");
                Console.ResetColor();

                (int, int)? tempDimensions = null;
                do
                {
                    Console.Write("Board dimensions (8,8) [x>1,y>1]: ");
                    input = Console.ReadLine().ToLower().Trim(); ;

                    if (input == "") break;

                    tempDimensions = ParsePair(input);
                }
                while (tempDimensions == null || tempDimensions.Value.Item1 < 2 || tempDimensions.Value.Item1 < 2);

                if (tempDimensions != null)
                {
                    dimensions = ((int, int))tempDimensions;
                }

                // get difficulty
                do
                {
                    Console.Write("Computer difficulty (10) [1-255]: ");
                    input = Console.ReadLine().ToLower().Trim();

                    if (input == "") break;
                }
                while ((!int.TryParse(input, out difficulty) || difficulty < 0 || difficulty > byte.MaxValue));

                // get move first or second
                do
                {
                    Console.Write("Would you (x's) like to go first, second, never? (F) [F/S or 1/2 or N]: ");
                    input = Console.ReadLine().ToLower().Trim();

                    if (input == "") break;
                }
                while (input != "f" && input != "s" && input != "1" && input != "2" && input != "n");

                if (input == "s" || input == "2")
                {
                    startTurn = Board.TURN.O;
                }
                else if (input == "n")
                {
                    isAIOnly = true;
                }

                Board board = new Board(startTurn, dimensions.Item1, dimensions.Item2);

                // main game loop
                while (true)
                {
                    // skip turn if there are no available moves
                    if (board.IsTerminal())
                    {
                        // or end the game if both players have no moves
                        if (board.IsTerminal(board.GetOpponent()))
                        {
                            break;
                        }

                        board.Advance();
                    }

                    Console.Clear();

                    Console.WriteLine(board);

                    if (board.turn == Board.TURN.X) // player turn
                    {
                        BoardMove move = null;

                        if (isAIOnly)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                            Console.WriteLine("Computer's turn...");
                            Console.ForegroundColor = ConsoleColor.Gray;
                            Console.Write("x's turn (computer): ");
                            (int, BoardMove) t = board.MiniMax(board, board.turn, difficulty);
                            Console.Write($"{t.Item2.x + 1}, {t.Item2.y + 1}\n");
                            Console.ResetColor();

                            board.MakeMove(t.Item2, true);
                            board.Advance();
                        }
                        else
                        {
                            Console.ForegroundColor = ConsoleColor.DarkGray;
                            Console.WriteLine("Enter x,y coordinates of your move, 'i' for info, or 'q' to quit");
                            Console.ResetColor();

                            do
                            {
                                Console.Write("x's turn (you): ");
                                input = Console.ReadLine().ToLower();

                                (int, int)? xy = ParsePair(input);

                                if (xy != null)
                                {
                                    move = ParseMove(((int, int))xy, board);
                                }
                            }
                            while ((move == null || !board.CheckMove(move)) && (input != "i" && input != "q"));

                            if (input == "q")
                            {
                                Quit();
                            }
                            else if (input == "i")
                            {
                                Console.Clear();
                                Console.ForegroundColor = ConsoleColor.Cyan;
                                Console.WriteLine("Reversi\n");
                                Console.ResetColor();

                                Console.WriteLine(
                                    "The player (you) is represented by x's (white) and the computer is represented by o's (black)\n" +
                                    "Enter a move so that there are o's between your new move and other 'x' pieces\n" +
                                    "Any 'o' pieces between your 'x' pieces will turn to 'x' pieces (diagonal included)\n" +
                                    "Once you complete your turn, the computer will do the same with their 'o' pieces\n" +
                                    "The player with the most pieces at the end of the game wins\n" +
                                    "Enter move coordinates in the form 'x,y', 'x, y', 'xy', or 'x y'\n" +
                                    "That is, the horizontal number followed by the vertical number\n"
                                );

                                Console.ForegroundColor = ConsoleColor.DarkGray;
                                Console.WriteLine("Press ENTER when finished");
                                Console.ResetColor();

                                Console.ReadLine();
                            }
                            else
                            {
                                board.MakeMove(move, true);
                                board.Advance();
                            }
                        }
                    }
                    else // computer turn
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGray;
                        Console.WriteLine("Computer's turn...");

                        Console.ForegroundColor = ConsoleColor.Gray;
                        Console.Write("o's turn (computer): ");

                        /*
                        foreach (BoardMove m in board.GetMoves())
                        {
                            Console.WriteLine(m);
                        }

                        Console.ReadLine();
                        */

                        (int, BoardMove) t = board.MiniMax(board, board.turn, difficulty);
                        Console.Write($"{t.Item2.x + 1}, {t.Item2.y + 1}\n");
                        Console.ResetColor();

                        board.MakeMove(t.Item2, true);

                        board.Advance();
                    }
                }

                Console.Clear();
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine(board);

                Board.TURN winner = board.GetWinner();

                switch (board.GetWinner())
                {
                    case Board.TURN.N:
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Tie!");
                        break;

                    case Board.TURN.X:
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("You won!");
                        break;

                    case Board.TURN.O:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Superior AI won!");
                        break;
                }

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write("\nPlay again? (N) [Y/N]: ");
                Console.ResetColor();
                input = Console.ReadLine().ToLower().Trim();
            }
            while (input == "y");
        }

        public static void Quit()
        {
            Console.WriteLine("\nQuitting...");
            Environment.Exit(0);
        }

        public static (int, int)? ParsePair(String input)
        {
            (int, int) pair;

            input = input.Replace(",", " ");
            String[] split = input.Split(" ", 2);

            if (split.Length != 2 ||
                !int.TryParse(split[0].Trim(), out pair.Item1) ||
                !int.TryParse(split[1].Trim(), out pair.Item2))
            {
                return null;
            }

            return pair;
        }

        public static BoardMove ParseMove((int, int) pair, Board board, Board.TURN turn = Board.TURN.X)
        {
            if (pair.Item1 <= 0 || pair.Item1 > board.width ||
                pair.Item2 <= 0 || pair.Item2 > board.height)
            {
                return null;
            }

            return new BoardMove(pair.Item1 - 1, pair.Item2 - 1, Board.TURN.X);
        }
    }
}
